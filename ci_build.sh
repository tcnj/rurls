#!/bin/sh

[[ -n "$1" ]] || exit 1

cd crates/$1

docker pull $CI_REGISTRY_IMAGE/$1:latest || true
docker build --cache-from $CI_REGISTRY_IMAGE/$1:latest --tag $CI_REGISTRY_IMAGE/$1:$CI_COMMIT_SHA --tag $CI_REGISTRY_IMAGE/$1:latest .
docker push $CI_REGISTRY_IMAGE/$1:$CI_COMMIT_SHA
docker push $CI_REGISTRY_IMAGE/$1:latest
