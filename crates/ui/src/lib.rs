#![allow(clippy::wildcard_imports)]

use seed::{prelude::*, *};

use std::iter;

use rand::distributions::Alphanumeric;
use rand::{thread_rng, Rng};

const GENERATION_ATTEMPTS: usize = 10;
const ATTEMPT_TIMEOUT: u32 = 1000;
const FRAMES_PER_CHARACTER: usize = 2;

// ------ ------
//     Init
// ------ ------

// `init` describes what should happen when your app started.
fn init(_: Url, orders: &mut impl Orders<Msg>) -> Model {
    orders.stream(streams::interval(100, || Msg::GotAnimation));
    Model {
        url: String::new(),
        id: Ok("…".into()),
        state: State::Idle,
    }
}

// ------ ------
//     Model
// ------ ------

// `Model` describes our app state.
struct Model {
    url: String,
    id: fetch::Result<String>,
    state: State,
}

enum State {
    Idle,
    Generating {
        failed_attempts: usize,
    },
    GotResult {
        link: Link,
        frame_counter: usize,
        characters_revealed: usize,
    },
}

impl State {
    fn is_working(&self) -> bool {
        if let State::Generating { .. } = self {
            return true;
        } else {
            return false;
        }
    }
}

#[derive(serde::Deserialize)]
struct Link {
    id: String,
    url: String,
}

// ------ ------
//    Update
// ------ ------

// `Msg` describes the different events you can modify state with.
enum Msg {
    GotURLInput(String),
    GotShortenPress,
    GotAttemptShortenAgain,
    GotShortenResponse(fetch::Result<Link>),
    GotAnimation,
}

// `update` describes how to handle each `Msg`.
fn update(msg: Msg, model: &mut Model, orders: &mut impl Orders<Msg>) {
    use Msg::*;
    log!("Update");
    match msg {
        GotURLInput(text) => model.url = text,
        GotShortenPress => {
            model.state = State::Generating { failed_attempts: 0 };
            send_shorten_request(model.url.clone(), orders);
        }
        GotAttemptShortenAgain => {
            if let State::Generating { .. } = model.state {
                send_shorten_request(model.url.clone(), orders);
            }
        }
        GotShortenResponse(Err(e)) => {
            if let State::Generating {
                ref mut failed_attempts,
            } = model.state
            {
                *failed_attempts += 1;

                if *failed_attempts < GENERATION_ATTEMPTS {
                    orders.perform_cmd(cmds::timeout(ATTEMPT_TIMEOUT, || {
                        Msg::GotAttemptShortenAgain
                    }));
                } else {
                    model.id = Err(e);
                    model.state = State::Idle;
                }
            }
        }
        GotShortenResponse(Ok(link)) => {
            model.state = State::GotResult {
                link,
                frame_counter: FRAMES_PER_CHARACTER,
                characters_revealed: 0,
            }
        }
        GotAnimation => match model.state {
            State::Idle => (),
            State::Generating { .. } => {
                let mut rng = thread_rng();

                model.id = Ok(iter::repeat(())
                    .map(|()| rng.sample(Alphanumeric))
                    .take(5)
                    .collect())
            }
            State::GotResult {
                ref link,
                ref mut frame_counter,
                ref mut characters_revealed,
            } => {
                if *frame_counter > FRAMES_PER_CHARACTER {
                    *frame_counter = 0;
                    *characters_revealed += 1;
                } else {
                    *frame_counter += 1;
                }

                let mut rng = thread_rng();

                model.id = Ok(link
                    .id
                    .chars()
                    .take(*characters_revealed)
                    .chain(
                        iter::repeat(())
                            .map(|()| rng.sample(Alphanumeric))
                            .take(5 - *characters_revealed),
                    )
                    .collect());

                if *characters_revealed >= 5 {
                    model.state = State::Idle;
                }
            }
        },
    }
}

fn send_shorten_request(url: String, orders: &mut impl Orders<Msg>) {
    async fn request(url: String) -> fetch::Result<Link> {
        Request::new(format!("https://api.dwns.xyz/api/v1/shorten?url={}", url))
            .method(Method::Post)
            .fetch()
            .await?
            .check_status()?
            .json()
            .await
    }
    orders.perform_cmd(async { Msg::GotShortenResponse(request(url).await) });
}

// ------ ------
//     View
// ------ ------

// `view` describes what to display.
fn view(model: &Model) -> Node<Msg> {
    let disabled_attr = if model.state.is_working() {
        attrs! {At::Disabled => "disabled"}
    } else {
        attrs! {}
    };

    div![
        C!["center", "center--vertical", "wrapper"],
        div![
            C!["center", "field-wrapper"],
            input![
                attrs! {
                    At::Type => "text",
                    At::Placeholder => "https://…",
                    At::Value => model.url.as_str(),
                },
                disabled_attr.clone(),
                input_ev(Ev::Input, |i| Msg::GotURLInput(i))
            ]
        ],
        div![
            C!["center", "button-wrapper"],
            button![
                disabled_attr,
                ev(Ev::Click, |_| Msg::GotShortenPress),
                "SHORTEN!"
            ],
        ],
        header![
            C!["center"],
            match model.id {
                Ok(ref id) => h1!["https://dwns.xyz/", id],
                Err(ref e) => h1![format!("{:?}", e)],
            }
        ]
    ]
}

// ------ ------
//     Start
// ------ ------

// (This function is invoked by `init` function in `index.html`.)
#[wasm_bindgen(start)]
pub fn start() {
    // Mount the `app` to the element with the `id` "app".
    App::start("app", init, update, view);
}
