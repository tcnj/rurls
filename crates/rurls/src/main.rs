use std::env;

use actix_web::{web::Data, App, HttpServer};

use rurls::context::Context;
use rurls::handlers::*;

#[actix_web::main]
async fn main() -> anyhow::Result<()> {
    env_logger::Builder::from_env(env_logger::Env::default().default_filter_or("info")).init();

    let listen = env::var("LISTEN").unwrap_or(String::from("127.0.0.1:8080"));

    HttpServer::new(|| {
        App::new()
            .app_data(Data::new(
                Context::build().expect("failed to build Context"),
            ))
            .service(info::handle)
            .service(redirector::handle)
            .service(redirector::handle_root)
    })
    .bind(listen.as_str())?
    .run()
    .await?;
    Ok(())
}
