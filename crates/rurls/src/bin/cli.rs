use std::env;

use redis::Commands;

use rurls::context::Context;

fn main() -> anyhow::Result<()> {
    let ctx = Context::build()?;

    let mut redis = ctx.redis.borrow_mut();

    let args = env::args().skip(1).collect::<Vec<_>>();

    match args.as_slice() {
        // Delete
        &[ref id] => {
            redis.del(format!("url:{}", id))?;
            println!("Key url:{} deleted successfully", id);
            Ok(())
        }

        &[ref id, ref url] => {
            redis.set(format!("url:{}", id), url)?;
            println!("Key url:{} successfully set to \"{}\"", id, url);
            Ok(())
        }

        // Incorrect args
        _ => Err(anyhow::Error::msg(
            "Expected one or two arguments: <ID> [URL]",
        )),
    }
}
