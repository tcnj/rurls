use std::env;

use actix_cors::Cors;
use actix_web::{web::Data, App, HttpServer};

use rurls::context::Context;
use rurls::handlers::{info, link};

#[actix_web::main]
async fn main() -> anyhow::Result<()> {
    env_logger::Builder::from_env(env_logger::Env::default().default_filter_or("info")).init();

    let listen = env::var("LISTEN").unwrap_or(String::from("127.0.0.1:8081"));

    HttpServer::new(|| {
        let cors = Cors::default()
            .allow_any_origin()
            .allow_any_method()
            .allow_any_header();

        App::new()
            .wrap(cors)
            .app_data(Data::new(
                Context::build().expect("failed to build Context"),
            ))
            .service(info::handle)
            .service(info::handle_root)
            .service(link::handle_get)
            .service(link::handle_shorten)
    })
    .bind(listen.as_str())?
    .run()
    .await?;
    Ok(())
}
