use std::fmt;

use actix_web::{http::StatusCode, HttpResponse, ResponseError};

#[derive(Debug)]
pub enum Error {
    Internal(anyhow::Error),
    NotFound,
    RateLimited,
    InvalidURL,
}

#[derive(serde::Serialize)]
pub struct ErrorResponse {
    error: bool,
    r#type: &'static str,
    friendly: &'static str,
}

impl ErrorResponse {
    fn new(type_: &'static str, friendly: &'static str) -> ErrorResponse {
        ErrorResponse {
            error: true,
            r#type: type_,
            friendly,
        }
    }
}

impl ResponseError for Error {
    fn status_code(&self) -> StatusCode {
        use Error::*;
        match self {
            Internal(_) => StatusCode::INTERNAL_SERVER_ERROR,
            NotFound => StatusCode::NOT_FOUND,
            RateLimited => StatusCode::TOO_MANY_REQUESTS,
            InvalidURL => StatusCode::BAD_REQUEST,
        }
    }

    fn error_response(&self) -> HttpResponse {
        use Error::*;
        let resp = match self {
            Internal(e) => {
                log::warn!("Internal error: {:?}", e);
                ErrorResponse::new("Internal", "Internal server error")
            }
            NotFound => ErrorResponse::new("NotFound", "Not found"),
            RateLimited => ErrorResponse::new(
                "RateLimited",
                "Access to this resource has been denied due to high demand",
            ),
            InvalidURL => ErrorResponse::new("InvalidURL", "An invalid URL was provided"),
        };

        HttpResponse::build(self.status_code()).json(resp)
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        log::warn!("Error handling request: {:?}", self);
        write!(f, "Internal error")
    }
}

impl From<anyhow::Error> for Error {
    fn from(e: anyhow::Error) -> Error {
        Error::Internal(e)
    }
}

impl From<url::ParseError> for Error {
    fn from(_: url::ParseError) -> Error {
        Error::InvalidURL
    }
}
