use std::cell::RefCell;
use std::env;

use redis::{Client, Connection};

pub struct Context {
    pub redis: RefCell<Connection>,
}

impl Context {
    pub fn build() -> anyhow::Result<Context> {
        let redis_address = env::var("REDIS_ADDR").unwrap_or(String::from("redis://127.0.0.1/"));

        let client = Client::open(redis_address.as_str())?;

        Ok(Context {
            redis: RefCell::new(client.get_connection()?),
        })
    }
}
