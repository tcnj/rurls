local last_time = redis.call("get", "shorten:last_time") or 0
local now = tonumber(redis.call("time")[1])

if now < last_time + 3 then
    return nil
end

redis.call("set", "shorten:last_time", now)

local charset = {}

-- qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890
for i = 48,  57 do table.insert(charset, string.char(i)) end
for i = 65,  90 do table.insert(charset, string.char(i)) end
for i = 97, 122 do table.insert(charset, string.char(i)) end

local function random_key(length)
    if length > 0 then
        return random_key(length - 1) .. charset[math.random(1, #charset)]
    else
        return ""
    end
end

local url = ARGV[1]
local seed = ARGV[2]

math.randomseed(tonumber(seed))

local attempts = 0
while true do
    local attempt = random_key(5)
    if redis.call("exists", "url:"..attempt) == 0 then
        redis.call("set", "url:"..attempt, url)
        return attempt
    else
        if redis.call("get", "url:"..attempt) == url then
            return attempt
        end

        attempts = attempts + 1
        if attempts > 10 then
            return {err="Unable to generate unique key"}
        end
    end
end
