use actix_web::{get, web::Data, web::HttpResponse, web::Path};
use redis::Commands;

use crate::context::Context;
use crate::error::Error;

fn handle_internal(id: impl AsRef<str>, ctx: &Context) -> Result<HttpResponse, Error> {
    let mut redis = ctx.redis.borrow_mut();

    let url: Option<String> = redis
        .get(format!("url:{}", id.as_ref()))
        .map_err(anyhow::Error::from)?;

    if let Some(url) = url {
        Ok(HttpResponse::TemporaryRedirect()
            .set_header("Location", url)
            .finish())
    } else {
        Ok(HttpResponse::NotFound().body("Link not found :("))
    }
}

#[get("/{id}")]
pub async fn handle(
    Path(id): Path<String>,
    ctx: Data<crate::context::Context>,
) -> Result<HttpResponse, Error> {
    let ctx = ctx.as_ref();
    handle_internal(id, ctx)
}

#[get("/")]
pub async fn handle_root(ctx: Data<crate::context::Context>) -> Result<HttpResponse, Error> {
    let ctx = ctx.as_ref();
    handle_internal("root", ctx)
}
