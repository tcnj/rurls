use actix_web::{get, web::Json, HttpResponse, Responder};
use serde::Serialize;

#[derive(Serialize)]
struct Info {
    name: &'static str,
    description: &'static str,
    version: &'static str,
}

impl Default for Info {
    fn default() -> Info {
        Info {
            name: env!("CARGO_PKG_NAME"),
            description: env!("CARGO_PKG_DESCRIPTION"),
            version: env!("CARGO_PKG_VERSION"),
        }
    }
}

#[get("/api/v1/info")]
pub async fn handle() -> impl Responder {
    Json(Info::default())
}

#[get("/")]
pub async fn handle_root() -> impl Responder {
    HttpResponse::TemporaryRedirect()
        .header("Location", "/api/v1/info")
        .finish()
}
