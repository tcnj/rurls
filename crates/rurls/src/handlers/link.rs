use actix_web::{get, post, web::Data, web::Path, web::Query, HttpResponse};
use redis::Commands;
use sha2::{Digest, Sha224};

use crate::error::Error;

#[derive(serde::Serialize)]
pub struct Link {
    id: String,
    url: String,
}

#[get("/api/v1/link/{id}")]
pub async fn handle_get(
    Path(id): Path<String>,
    ctx: Data<crate::context::Context>,
) -> Result<HttpResponse, Error> {
    let ctx = ctx.as_ref();
    let mut redis = ctx.redis.borrow_mut();

    let maybe_url: Option<String> = redis
        .get(format!("url:{}", id))
        .map_err(anyhow::Error::from)?;

    if let Some(url) = maybe_url {
        Ok(HttpResponse::Ok().json(Link { id, url }))
    } else {
        Err(Error::NotFound)
    }
}

#[derive(serde::Deserialize)]
pub struct ShortenQuery {
    url: String,
}

#[post("/api/v1/shorten")]
pub async fn handle_shorten(
    Query(query): Query<ShortenQuery>,
    ctx: Data<crate::context::Context>,
) -> Result<HttpResponse, Error> {
    const LUA_SHORTEN: &'static str = include_str!("link/shorten.lua");

    let url = query.url;

    let parsed_url = url::Url::parse(url.as_str())?;
    match parsed_url.scheme() {
        "http" => (),
        "https" => (),
        _ => return Err(Error::InvalidURL),
    }

    let digest = Sha224::digest(url.as_bytes());
    let mut digest = digest.iter();
    let seed = u32::from_le_bytes([
        *digest.next().unwrap(),
        *digest.next().unwrap(),
        *digest.next().unwrap(),
        *digest.next().unwrap(),
    ]);

    let ctx = ctx.as_ref();
    let mut redis = ctx.redis.borrow_mut();

    let maybe_id: Option<String> = redis::cmd("EVAL")
        .arg(LUA_SHORTEN)
        .arg(0)
        .arg(url.as_str())
        .arg(seed)
        .query(&mut *redis)
        .map_err(anyhow::Error::from)?;

    if let Some(id) = maybe_id {
        Ok(HttpResponse::Created().json(Link { id, url }))
    } else {
        Err(Error::RateLimited)
    }
}
